package cloud.appuio.fortune;

public class FortuneData {
    public int number;
    public String message;
    public String version;
    public String hostname;

    @Override
    public String toString() {
        return String.format("Fortune %s cookie of the day #%d:\n\n%s", version, number, message);
    }
}
