package cloud.appuio.fortune;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeFortuneResourceIT extends FortuneResourceTest {

    // Execute the same tests but in native mode.
}